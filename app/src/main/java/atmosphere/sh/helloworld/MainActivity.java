package atmosphere.sh.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    TextView resultA,resultB;

    int a = 0, b = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initial();
    }

    public void initial ()
    {
        resultA = findViewById(R.id.resultA);
        resultB = findViewById(R.id.resultB);
    }

    public void view(View view)
    {
        switch (view.getId())
        {
            case R.id.A3 :
                a = a + 3;
                break;
            case R.id.A2 :
                a = a + 2;
                break;
            case R.id.A1:
                a = a + 1;
                break;
            case R.id.B3:
                b = b + 3;
                break;
            case R.id.B2:
                b = b + 2;
                break;
            case R.id.B1:
                b = b + 1;
                break;
            case R.id.reset :
                a = 0;
                b = 0;
                break;
        }
        resultA.setText(a + "");
        resultB.setText(b + "");
    }
}
